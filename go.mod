module gitlab.com/saintaardvark/arduino-wx-go-logger/wx-logger

go 1.14

require (
	github.com/influxdata/influxdb v1.8.1
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	golang.org/x/sys v0.0.0-20200625212154-ddb9806d33ae // indirect
)
